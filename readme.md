online_store_restful_API
========================

REST API implementation in Django for managing products of an online store

How to run?
------------
* `git clone https://namc@bitbucket.org/namc/online_store_api.git`
* `cd online_store_api`
* `sudo pip install virtualenv`
* `virtualenv wingify`
* `source wingify/bin/activate`
* `pip install -r requirement.txt` 
* `python manage.py syncdb`
* Load database with some initial data as fixtures :
     * `python manage.py loaddata users` 
        * will load 2 dummy users with usernames: _userAdmin_, _userPass_ and password: _testpass_
     * `python manage.py loaddata products` (will load a product list)
* `python manage.py runserver` 
    * test your application running on [http://127.0.0.1:8000/](http://127.0.0.1:8000/)

Test
----
* Test the code by `python manage.py test`
* Tests are written in `app/tests.py`


API Requests
------------
We can make GET, POST, PUT, DELETE requests using httpie which would be already installed if the requirements installation process was successful.

### Authentication ###
This API supports basic(username:password) authentication and a token based authentication.

* Basic authentication using httpie: `http -a userAdmin:testpass http://127.0.0.1:8000/`

* Token based authentication using httpie: `http http://127.0.0.1:8000/ 'Authorization: Token <token>'`

In this project GET, POST, PUT, DELETE requests require authentication.

### GET ###
Making a request using any of these two methods will give a complete list of products

* `http -a userAdmin:testpass GET http://127.0.0.1:8000/products/`

suppose the token for user _userAdmin_ is `b13a2378a575338f7384d248934ad5e31ab957ab3` 

* `http GET http://127.0.0.1:8000/products/ 'Authorization: Token b13a2378a575338f7384d248934ad5e31ab957ab3'`

Example:

    [
      {
        "model": "app.product",
        "pk": 1,
        "fields": {
          "name": "Shahenaz Blue Poly Silk Cushion Cover",
          "description": "The cover is made of cotton and measures 16 x 16 inches in size and the package contains one cushion cover.",
          "price": 200,
          "category": "Furnishing",
          "owner": 2,
          "created": "2016-01-14T18:05:36.359Z"
        }
      },
      {
        "model": "app.product",
        "pk": 2,
        "fields": {
          "name": "Stoneware Cups & Saucers",
          "description": "Unravel India Studio Stoneware Cups & Saucers - Set of 6",
          "price": 100,
          "category": "Dining",
          "owner": 2,
          "created": "2016-01-14T18:04:36.359Z"
        }
      },
      {
        "model": "app.product",
        "pk": 3,
        "fields": {
          "name": "Aesthetics Home Solution White Glass Chandelier",
          "description": "These wall lightings are not only a piece of beautiful décor, but also one of the best option to lighten-up your room space",
          "price": 100,
          "category": "Lamps",
          "owner": 3,
          "created": "2016-01-14T18:03:36.359Z"
        }
      }
    ]

### POST ###
Adding a new item:
* `http -a userPass:testpass POST http://127.0.0.1:8000/products/ name="product1" price=200`

OR
* `http POST http://127.0.0.1:8000/products/ name="product1" price=200 'Authorization: Token b13a2378a575338f7384d248934ad5e31ab957ab3'`

### PUT ###
Update an existing item:
* `http -a userAdmin:testpass PUT http://127.0.0.1:8000/products/2/ name="UpdatedProduct" price=200`

OR
* `http PUT http://127.0.0.1:8000/products/2/ name="UpdatedProduct" price=200 'Authorization: Token b13a2378a575338f7384d248934ad5e31ab957ab3'`

if the user is authenticated and is the owner of the product entry, then it will successfully update the item details otherwise not.

### DELETE ###
Remove an item:
* `http -a userPass:testpass DELETE http://127.0.0.1:8000/products/1/`

OR
* `http DELETE http://127.0.0.1:8000/products/1/ 'Authorization: Token b13a2378a575338f7384d248934ad5e31ab957ab3'`

if the user is authenticated and is the owner of the product entry, then it will successfully remove the item otherwise not.

### GET ###
Search an item using query parameter `q`
* `http -a userPass:testpass GET http://127.0.0.1:8000/products/?q="Cups"`

OR

* `http GET 127.0.0.1:8000/products/?q="Cups" 'Authorization: Token b13a2378a575338f7384d248934ad5e31ab957ab3'`

This will match the query string with the name of the products and return appropriate result
Example:

    [
        {
        "model": "app.product",
        "pk": 2,
        "fields": {
          "name": "Stoneware Cups & Saucers",
          "description": "Unravel India Studio Stoneware Cups & Saucers - Set of 6",
          "price": 100,
          "category": "Dining",
          "owner": 2,
          "created": "2016-01-14T18:04:36.359Z"
        }
    ]