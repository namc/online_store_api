from django.test import TestCase, Client
from django.contrib.auth.models import User

from app.views import product_detail, ProductList
from app.models import Product

from rest_framework import status
from rest_framework.test import APIRequestFactory, APIClient, force_authenticate
import json
from collections import OrderedDict

class ProductTest(TestCase):

    fixtures = ['test_users.json', 'test_products.json']

    def test_get_product_no_auth(self):

        client = APIClient()
        response = self.client.get('/products/1/')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data, \
                {"detail":"Authentication credentials were not provided."})

    def test_get_product_with_auth(self):

        c = APIClient()
        c.login(username='userAdmin', password='testpass')
        response = c.get('/products/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.content, '{"id":1,"owner":"userAdmin","name":"Shahenaz Blue Poly Silk Cushion Cover","description":"The cover is made of cotton and measures 16 x 16 inches in size and the package contains one cushion cover.","price":200,"category":"Furnishing","image":""}')

    def test_post_product_no_auth(self):

        client = APIClient()
        data = {'name': 'Tezerac Natural Wooden Basket', 'description' : 'This basket is ideal to store your bathroom essentials, from toiletries to accessories or your magazines.' , 'price': 500, 'category' : 'Housekeeping', 'owner':1}
        response = client.post('/products/', data, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data, \
                {"detail":"Authentication credentials were not provided."})

    def test_post_product_with_auth(self):
        client = APIClient()
        client.login(username='userAdmin', password='testpass')
        data = {'name': 'Tezerac Natural Wooden Basket', 'description' : 'This basket is ideal to store your bathroom essentials, from toiletries to accessories or your magazines.' , 'price': 500, 'category' : 'Housekeeping', 'owner':1}
        response = client.post('/products/', data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Product.objects.count(), 2)

    def test_put_product_no_auth(self):

        client = APIClient()
        data = {'name': 'Yaanai Uruli by Greymode', 'description' : 'This unique home decor can be used as a side table, corner table, pedestal, side stool or seat. Available in 2 different sizes and 3 different colours.' , 'price': 500, 'category' : 'Garden', 'owner':1}
        response = client.post('/products/1/', data, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data, \
                {"detail":"Authentication credentials were not provided."})

    def test_put_product_with_auth(self):
        client = APIClient()
        client.login(username='userAdmin', password='testpass')
        data = {'name': 'Yaanai Uruli by Greymode', 'description' : 'This unique home decor can be used as a side table, corner table, pedestal, side stool or seat. Available in 2 different sizes and 3 different colours.' , 'price': 500, 'category' : 'Garden', 'owner':1}
        response = client.put('/products/1/', data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Product.objects.count(), 1)

    def test_delete_product_no_auth(self):

        client = APIClient()
        response = client.delete('/products/1/')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data, \
                {"detail":"Authentication credentials were not provided."})

    def test_delete_product_with_auth(self):

        client = APIClient()
        client.login(username='userAdmin', password='testpass')
        response = client.delete('/products/1/')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Product.objects.count(), 0)

